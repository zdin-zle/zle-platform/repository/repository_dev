import ckan.plugins.toolkit as tk


@tk.auth_allow_anonymous_access
def newextension_get_sum(context, data_dict):
    return {"success": True}


def get_auth_functions():
    return {
        "newextension_get_sum": newextension_get_sum,
    }
