"""Tests for views.py."""

import pytest

import ckanext.newextension.validators as validators


import ckan.plugins.toolkit as tk


@pytest.mark.ckan_config("ckan.plugins", "newextension")
@pytest.mark.usefixtures("with_plugins")
def test_newextension_blueprint(app, reset_db):
    resp = app.get(tk.h.url_for("newextension.page"))
    assert resp.status_code == 200
    assert resp.body == "Hello, newextension!"
