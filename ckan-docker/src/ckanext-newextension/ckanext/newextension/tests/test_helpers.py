"""Tests for helpers.py."""

import ckanext.newextension.helpers as helpers


def test_newextension_hello():
    assert helpers.newextension_hello() == "Hello, newextension!"
