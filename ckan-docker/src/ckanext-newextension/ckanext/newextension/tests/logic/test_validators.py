"""Tests for validators.py."""

import pytest

import ckan.plugins.toolkit as tk

from ckanext.newextension.logic import validators


def test_newextension_reauired_with_valid_value():
    assert validators.newextension_required("value") == "value"


def test_newextension_reauired_with_invalid_value():
    with pytest.raises(tk.Invalid):
        validators.newextension_required(None)
