import click


@click.group(short_help="newextension CLI.")
def newextension():
    """newextension CLI.
    """
    pass


@newextension.command()
@click.argument("name", default="newextension")
def command(name):
    """Docs.
    """
    click.echo("Hello, {name}!".format(name=name))


def get_commands():
    return [newextension]
