from flask import Blueprint


newextension = Blueprint(
    "newextension", __name__)


def page():
    return "Hello, newextension!"


newextension.add_url_rule(
    "/newextension/page", view_func=page)


def get_blueprints():
    return [newextension]
