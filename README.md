# CKAN: The Open Source Data Portal Software
CKAN is the world’s leading open-source data portal platform. CKAN makes it easy to publish, share and work with data. It's a data management system that provides a powerful platform for cataloging, storing and accessing datasets with a rich front-end, full API (for both data and catalog), visualization tools and more. Read more at [ckan.org](https://ckan.org/).

This document provides an overview of the documents for the installation and development/deployment of CKAN. The respective documents are linked.

<br>

### Table of contents
- [How to install CKAN](#how-to-install-ckan)
  - [Source](#source)
  - [Docker](#docker)
- [Deployment](#deployment)
- [Nice to know/see](#nice-to-knowsee)


<!-- ------------------------------------------ -->
## How to install CKAN
When to choose between Source and Docker Installation?

### Source
You should install CKAN from source if:
- You want to install CKAN on a 32-bit computer, or
- You want to install CKAN on a different version of Ubuntu, not 20.04 or 22.04, or 
- You want to install CKAN on another operating system (eg. RHEL, CentOS, OS X), or 
- You want to run multiple CKAN websites on the same server, or 
- You want to install CKAN for development


&rarr; [Installation of CKAN (2.9.8 and older)](../guides/Installation-CKAN-2.9.8.md) \
&rarr; [Installation of CKAN (2.10.0 and newer)](../guides/Installation-CKAN-2.10.0.md)

### Docker
The [ckan-docker](https://github.com/ckan/ckan-docker) repository contains the necessary scripts and images to install CKAN using Docker Compose. It provides a clean and quick way to deploy a standard CKAN instance pre-configured with the [Filestore](https://docs.ckan.org/en/2.10/maintaining/filestore.html) and [DataStore](https://docs.ckan.org/en/2.10/maintaining/datastore.html) extension. It also allows the addition (and customization) of extensions. The emphasis leans more towards a Development environment, however the base install can be used as the foundation for progressing to a Production environment. Please note that a fully-fledged CKAN Production system using Docker containers is beyond the scope of the provided setup.

You should install CKAN from Docker Compose if:
- You want to install CKAN with less effort than a source install and more flexibility than a package install, or 
- You want to run or even develop extensions with the minimum setup effort, or 
- You want to see whether and how CKAN, Docker and your respective infrastructure will fit together.

&rarr; [Installation of CKAN with Docker](../guides/Installation-CKAN-Docker.md)



<!-- ------------------------------------------ -->
## Deployment (Source Installation)
&rarr; [Deployment of CKAN](../guides/Deployment-CKAN.md)



<!-- ------------------------------------------ -->
## Nice to know/see
How to update and upgrade CKAN:

&rarr; [Update of CKAN](../guides/Update-of-CKAN.md)



Some usefull fast commands for CKAN (e.g. how to start CKAN):

&rarr; [Fast Commands](../guides/fast-commands.md)