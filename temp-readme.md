# How to install CKAN

#### Table of contents
1. [Install CKAN from Source](#install-ckan-from-source)
    1. [Install the required packages](#1-install-the-required-packages)
    2. [Install CKAN into a Python virtual environment](#2-install-ckan-into-a-python-virtual-environment)
    3. [Setup a PostgreSQL database](#3-setup-a-postgresql-database)
    - [Install and configure Solr](#4-install-and-configure-solr)
    - [Create a CKAN config file](#5-create-a-ckan-config-file)
    - [Link to who.ini](#6-link-to-whoini)
    - [Create database tables](#7-create-database-tables)
    - [Set up the DataStore](#8-set-up-the-datastore)
    - [You're done!](#9-youre-done)
2. [Install CKAN from Docker](#install-ckan-from-docker)
    - [Enviroment -> Set up Docker](#enviroment-->-set-up-docker)
    - [CKAN Source](#ckan-source)
    - [Build Docker images](#build-docker-images)
    - [Datastore and datapusher](#datastore-and-datapusher)
    - [Create CKAN admin user](#create-ckan-admin-user)
    - [Migrate data](#migrate-data)
    - [Add extensions](#add-extensions)
    - [Environment variables](#environment-variables)
    - [Steps towards production](#steps-towards-production)
3. [How to restart CKAN](#how-to-restart-ckan)
4. [Fast Commands](#fast-comands)
5. [How to Update CKAN Repository](#how-to-update-ckan-repository)
6. [Update CKAN (Source)](#update-ckan-source)



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## Install CKAN from source
<!------------------------------------------>
[Installing CKAN from source](http://docs.ckan.org/en/2.9/maintaining/installing/install-from-source.html)

- For Python 3 installations, the minimum Python version required is 3.6.
- Ubuntu 20.04. or Ubuntu 18.04. is needed (tested with 20.04.)



### 1. Install the required packages
If you’re using a Debian-based operating system (such as Ubuntu) install the required packages with this command:
```
sudo apt-get install python3-dev postgresql libpq-dev python3-pip python3-venv git-core solr-jetty openjdk-8-jdk redis-server
```

| Package       | Description                                                                           |
| ---           | ---                                                                                   |
| Python        | The Python programming language, v3.6 or newer (or v2.7)                              |
| PostgresSQL   | The PostgreSQL database system, v9.5 or newer                                         |
| libpq         | The C programmer’s interface to PostgreSQL                                            |
| pip           | A tool for installing and managing Python packages                                    |
| python3-venv  | The Python3 virtual environment builder (or for Python 2 use ‘virtualenv’ instead)    |
| Git           | A distributed version control system                                                  |
| Apache Solr   | A search platform                                                                     |
| Jetty         | An HTTP server (used for Solr)                                                        |
| OpenJDK JDK   | The Java Development Kit (used by Jetty)                                              |
| Redis         | An in-memory data structure store                                                     |



<!-- blank line -->
<br>
<!-- blank line -->

### 2. Install CKAN into a Python virtual environment
> **Tipp:**
If you’re installing CKAN for development and want it to be installed in your home directory, you can symlink the directories used in this documentation to your home directory. This way, you can copy-paste the example commands from this documentation without having to modify them, and still have CKAN installed in your home directory:
>

```
mkdir -p ~/ckan/lib
sudo ln -s ~/ckan/lib /usr/lib/ckan
mkdir -p ~/ckan/etc
sudo ln -s ~/ckan/etc /etc/ckan
```



<!-- blank line -->
<br>
<!-- blank line -->

#### a. Create a Python virtual environment (virtualenv) to install CKAN into, and activate it
```
sudo mkdir -p /usr/lib/ckan/default
sudo chown `whoami` /usr/lib/ckan/default
python3 -m venv /usr/lib/ckan/default
. /usr/lib/ckan/default/bin/activate
```

> The final command above activates your virtualenv. The virtualenv has to remain active for the rest of the installation and deployment process, or commands will fail. You can tell when the virtualenv is active because its name appears in front of your shell prompt, something like this:\
    ```
    (default) $ _
    ```\
    For example, if you logout and login again, or if you close your terminal window and open it again, your virtualenv will no longer be activated. You can always reactivate the virtualenv with this command:\
    ```
    . /usr/lib/ckan/default/bin/activate
    ```



<!-- blank line -->
<br>
<!-- blank line -->

#### b. Install the recommended `setuptools` version and up-to-date pip
```
pip install setuptools==44.1.0
pip install --upgrade pip
```



<!-- blank line -->
<br>
<!-- blank line -->

#### c. Install the CKAN source code into your virtualenv
To install the current stable release of CKAN (CKAN 2.9.7), run:
```
pip install -e 'git+https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git@ckan-2.9.8#egg=ckan[requirements]'
```



<!-- blank line -->
<br>
<!-- blank line -->

#### d. Deactivate and reactivate your virtualenv, to make sure you’re using the virtualenv’s copies of commands like ckan rather than any system-wide installed copies
```
deactivate
. /usr/lib/ckan/default/bin/activate
```



<!-- blank line -->
<br>
<!-- blank line -->

### 3. Setup a PostgreSQL database
Check that PostgreSQL was installed correctly by listing the existing databases:
```
sudo -u postgres psql -l
```

Check that the encoding of databases is `UTF8`, if not you might find issues later on with internationalisation. Since changing the encoding of PostgreSQL may mean deleting existing databases, it is suggested that this is fixed before continuing with the CKAN install.

Next you’ll need to create a database user if one doesn’t already exist. Create a new PostgreSQL user called ckan_default, and enter a password for the user when prompted. You’ll need this password later:
```
sudo -u postgres createuser -S -D -R -P ckan_default
```

Create a new PostgreSQL database, called ckan_default, owned by the database user you just created:
```
sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
```



<!-- blank line -->
<br>
<!-- blank line -->

### 4. Install and configure Solr
Install Solr, running this command in a terminal:
```
sudo apt install -y solr-tomcat
```



<!-- blank line -->
<br>
<!-- blank line -->

#### a. Change the default port Tomcat runs on (8080) to the one expected by CKAN.
To do so change the following line in the `/etc/tomcat9/server.xml` file:
-> `sudo nano /etc/tomcat9/server.xml`

```
From:
  <Connector port="8080" protocol="HTTP/1.1"

To:
  <Connector port="8983" protocol="HTTP/1.1"
```



<!-- blank line -->
<br>
<!-- blank line -->

#### b. Replace the default `schema.xml` file with a symlink to the CKAN schema file included in the sources.
```
sudo mv /etc/solr/conf/schema.xml /etc/solr/conf/schema.xml.bak
sudo ln -s /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
```
If this causes an error in the next step, copy the file by
```
sudo cp /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
```



<!-- blank line -->
<br>
<!-- blank line -->

#### c. Now restart Solr (use `tomcat8` on older Ubuntu versions):
```
sudo service tomcat9 restart
```
Check that Solr is running by opening http://localhost:8983/solr/

> localhost probably needs to be changed into an ip adress



<!-- blank line -->
<br>
<!-- blank line -->

### 5. Create a CKAN config file
Create a directory to contain the site’s config files:
```
sudo mkdir -p /etc/ckan/default
sudo chown -R `whoami` /etc/ckan/
```

Create the CKAN config file:
```
ckan generate config /etc/ckan/default/ckan.ini
```



<!-- blank line -->
<br>
<!-- blank line -->

In the `ckan.ini` following options need to be changed:
-> `sudo nano /etc/ckan/default/ckan.ini` 

#### **sqlalchemy.url**
This should refer to the database we created in [3. Setup a PostgreSQL database](#3-setup-a-postgresql-database) database above:
```
sqlalchemy.url = postgresql://ckan_default:pass@localhost/ckan_default
```
Replace `pass` with the password that you created in 3. Setup a PostgreSQL database above.



<!-- blank line -->
<br>
<!-- blank line -->

#### **site_id**
Each CKAN site should have a unique `site_id`, for example:
```
ckan.site_id = default
```



<!-- blank line -->
<br>
<!-- blank line -->

#### **site_url**
Provide the site’s URL (used when putting links to the site into the FileStore, notification emails etc). For example:
```
ckan.site_url = http://localhost:5000
alternate: ckan.site_url = http://demo.ckan.org
```
`demo.ckan.org` is only a placeholder



<!-- blank line -->
<br>
<!-- blank line -->

#### **sorl_url**
Set the solr url with the new Port:
```
solr_url=http://127.0.0.1:8983/solr
```



<!-- blank line -->
<br>
<!-- blank line -->

### 6. Link to `who.ini`
`who.ini` (the Repoze.who configuration file) needs to be accessible in the same directory as your CKAN config file, so create a symlink to it:
```
ln -s /usr/lib/ckan/default/src/ckan/who.ini /etc/ckan/default/who.ini
```



<!-- blank line -->
<br>
<!-- blank line -->

### 7. Create database tables
Now that you have a configuration file that has the correct settings for your database, you can create the database tables:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini db init
```
You should see `Initialising DB: SUCCESS`

>  **Tipp:** If the command prompts for a password it is likely you haven’t set up the `sqlalchemy.url` option in your CKAN configuration file properly. See [5. Create a CKAN config file](#5-create-a-ckan-config-file).
>



<!-- blank line -->
<br>
<!-- blank line -->

### 8. Set up the DataStore
> TODO



<!-- blank line -->
<br>
<!-- blank line -->

### 9. You’re done!
You can now run CKAN from the command-line. This is a simple and lightweight way to serve CKAN that is useful for development and testing:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini run
```
Open [http://localhost:5000/](http://localhost:5000/) in a web browser, and you should see the CKAN front page.

Now that you’ve installed CKAN, you should:
- Run CKAN’s tests to make sure that everything’s working, see [Testing CKAN](http://docs.ckan.org/en/2.9/contributing/test.html).
- If you want to use your CKAN site as a production site, not just for testing or development purposes, then deploy CKAN using a production web server such as uWSGI or Nginx. See [Deploying a source install](http://docs.ckan.org/en/2.9/maintaining/installing/deployment.html).
- Begin using and customizing your site, see [Getting started](http://docs.ckan.org/en/2.9/maintaining/getting-started.html).



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## Install CKAN from Docker
<!------------------------------------------>
[Installing CKAN from Docker](https://docs.ckan.org/en/2.9/maintaining/installing/install-from-docker-compose.html)


### Enviroment -> Set up Docker
#### Set up Docker repository
Before you install Docker Engine for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

##### a. Update the `apt` package index and install packages to allow apt to use a repository over HTTPS:
```
sudo apt-get update
sudo  apt-get install \
      ca-certificates \
      curl \
      gnupg \
      lsb-release
```

##### b. Add Docker’s official GPG key:
```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

##### c. Use the following command to set up the repository:
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```


#### Install Docker Engine

##### a. Update the `apt` package index:
```
sudo apt-get update
```

##### b. Install Docker Engine, containerd, and Docker Compose.
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

##### c. Verify that the Docker Engine installation is successful by running the hello-world image:
```
sudo docker run hello-world
```


#### Docker Engine post-installation steps

##### a. Create the `docker` group.
```
sudo groupadd docker
```

##### b. Add your user to the `docker` group.
```
sudo usermod -aG docker $USER
```

##### c. Log out and log back in so that your group membership is re-evaluated.
> If you’re running Linux in a virtual machine, it may be necessary to restart the virtual machine for changes to take effect.

```
newgrp docker
```

##### d. Verify that you can run `docker` commands without `sudo`.
```
docker run hello-world
```

The output should be following:
```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

```


#### Install the Compose plugin
> From the end of June 2023 Compose V1 won’t be supported anymore and will be removed from all Docker Desktop versions. \
\
Make sure you switch to [Compose V2](https://docs.docker.com/compose/compose-file/) with the `docker compose` CLI plugin or by activating the Use **Docker Compose V2** setting in Docker Desktop. For more information, see the [Evolution of Compose](https://docs.docker.com/compose/compose-v2/)


##### a. Update the package index, and install the latest version of Docker Compose:
```
sudo apt-get update
sudo apt-get install docker-compose-plugin
sudo apt install docker-compose
```

##### b. Verify that Docker Compose is installed correctly by checking the version.
```
docker-compose version
```
Output should be `Docker Compose version vN.N.N` where `vN.N.N` should be the latest version.

#### CKAN Source
For the installation of CKAN we need to clone the repository of CKAN into a directory of our choice:
```
cd /path
git clone https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git
```

Rename the directory to ckan:
```
mv repository_dev/ ckan/
```

This will use the latest CKAN master, which may not be stable enough for production use. To use a stable version, checkout the respective tag, e.g.:
```
git checkout tags/ckan-2.9.7
```


### Build Docker images
In this step we will build the Docker images and create Docker data volumes with user-defined, sensitive settings (e.g. database passwords).

#### a. Sensitive settings and environment variables
Copy `contrib/docker/.env.template` to `contrib/docker/.env` and follow instructions within to set passwords and other sensitive or user-defined variables.
```
mv contrib/docker/.env.template contrib/docker/.env
```
with `sudo nano contrib/docker/.env` is the file openable


#### b. Build the images
Inside the CKAN directory:
```
cd contrib/docker
docker-compose up -d --build
```

For the remainder of this chapter, we assume that docker-compose commands are all run inside `contrib/docker`, where `docker-compose.yml` and `.env` are located (the `.env` might be invisible but it's there).

On first runs, the postgres container could need longer to initialize the database cluster than the ckan container will wait for. This time span depends heavily on available system resources. If the CKAN logs show problems connecting to the database, restart the ckan container a few times:
```
docker-compose restart ckan
docker ps | grep ckan
docker-compose logs -f ckan
```



### Datastore and datapusher
> TODO


### Create CKAN admin user
> TODO


### Migrate data
> TODO


### Add extensions
> TODO


### Environment variables
> TODO


### Steps towards production
> TODO




<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## How to restart CKAN
<!------------------------------------------>
change the path:
```
. /usr/lib/ckan/default/bin/activate

```

Start ckan:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini run
```

> CKAN should start after.



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## Fast comands
<!------------------------------------------>

### remove and add who.ini
```
rm /etc/ckan/default/who.ini
ln -s /usr/lib/ckan/default/src/ckan/who.ini /etc/ckan/default/who.ini
```


<!-- blank line -->
<br>
<!-- blank line -->

### kill the used port ckan is running on
```
sudo kill -9 $(sudo lsof -t -i:<port>)
```



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## How to Update CKAN Repository
<!------------------------------------------>
First you need to check if the CKAN GitHub is linked as a upstream with `git remote -v`. It should show following output:
```
origin          https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git (fetch)
origin          https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git (push)
upstream        https://github.com/ckan/ckan.git (fetch)
upstream        https://github.com/ckan/ckan.git (push)

```

If not, add CKAN as a upstream repository:
```
git remote add upstream https://github.com/ckan/ckan.git
```

Next step, fetch the changes of CKAN and create a local branch which has the same name as the tag version of ckan. In this step it is important that the names are the same.
```
git fetch upstream
git checkout -b ckan-VERSION ckan-VERSION
```

In the last step, the new branch needs to be pushed to the repository.
```
git branch --set-upstream-to=origin/ckan-VERSION ckan-VERSION
git push origin
```
if `git push origin` gives an error try to delete the tag with `git tag -d ckan-VERSION` and try again



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## Update CKAN (Source)
<!------------------------------------------>
This document describes the different types of CKAN release, and explains how to upgrade a site to a newer version of CKAN.



<!-- blank line -->
<br>
<!-- blank line -->

### Backup your databas
> TODO



<!-- blank line -->
<br>
<!-- blank line -->

### Upgrade CKAN

#### **Step 1**
Check the Changelog for changes regarding the required 3rd-party packages and their minimum versions (e.g. web, database and search servers) and update their installations if necessary.


#### **Step 2**
Activate your virtualenv and switch to the ckan source directory, e.g.:
```
. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
```


#### **Step 3**
Checkout the new CKAN version from git, for example:
```
git fetch
git checkout ckan-VERSION
```
If a normal checkout is not possible do a force checkout
```
git checkout -f ckan-VERSION
```

If you have any CKAN extensions installed from source, you may need to checkout newer versions of the extensions at this point as well. Refer to the documentation for each extension.


#### **Step 4**
Update CKAN’s dependencies:
```
pip install --upgrade -r requirements.txt
```


#### **Step 5**
Register any new or updated plugins:
```
python setup.py develop
```


#### **Step 6**
If there have been changes in the Solr schema (check the Changelog to find out) you need to restart tomcat for the changes to take effect:
```
sudo service tomcat9 restart
```


#### **Step 7**
If there have been changes in the database schema (check the Changelog to find out) you need to upgrade your database schema.


#### **Step 8**
If new configuration options have been introduced (check the Changelog to find out) then check whether you need to change them from their default values. See Configuration Options for details.



#### **Step 9**
Rebuild your search index by running the `ckan search-index rebuild` command:
```
ckan -c /etc/ckan/default/ckan.ini search-index rebuild -r
```


#### **Step 10**
Finally, restart your web server!
```
ckan -c /etc/ckan/default/ckan.ini run
```

> You’re done!



<!-- blank line -->
<br>
<br>
<!-- blank line -->

<!------------------------------------------>
## CKAN releases
<!------------------------------------------>
CKAN follows a predictable release cycle so that users can depend on stable releases of CKAN, and can plan their upgrades to new releases.

Each release has a version number of the form `M.m` (eg. 2.1) or `M.m.p` (eg. 1.8.2), where `M` is the **major version**, `m` is the **minor version** and `p` is the **patch version** number. There are three types of release:


#### **Major Releases**
Major releases, such as CKAN 1.0 and CKAN 2.0, increment the major version number. These releases contain major changes in the CKAN code base, with significant refactorings and breaking changes, for instance in the API or the templates. These releases are very infrequent.


#### **Minor Releases**
Minor releases, such as CKAN 1.8 and CKAN 2.1, increment the minor version number. These releases are not as disruptive as major releases, but backwards-incompatible changes may be introduced in minor releases. The Changelog will document any breaking changes. We aim to release a minor version of CKAN roughly every three months.


#### **Patch Releases**
Patch releases, such as CKAN 1.8.1 or CKAN 2.0.1, increment the patch version number. These releases do not break backwards-compatibility, they include only bug fixes and security fixes, ensured to be non-breaking. Patch releases do not contain:
- Database schema changes or migrations
- Function interface changes
- Plugin interface changes
- New dependencies (unless absolutely necessary)
- Big refactorings or new features in critical parts of the code

