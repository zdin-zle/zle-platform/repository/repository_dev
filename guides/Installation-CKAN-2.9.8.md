# Installation of CKAN 2.9.8 and lower (Source)
<!-- ------------------------------------------ -->
This section describes how to install CKAN from source. Although [Installing CKAN from package](https://docs.ckan.org/en/2.9/maintaining/installing/install-from-package.html) is simpler, it requires Ubuntu 18.04 64-bit or Ubuntu 16.04 64-bit. Installing CKAN from source works with other versions of Ubuntu and with other operating systems (e.g. RedHat, Fedora, CentOS, OS X). If you install CKAN from source on your own operating system, please share your experiences on our How to Install CKAN wiki page.

<br>


### Table of contents
- [Requirements](#requirements-for-ckan)
- [Install the required packages](#1-install-the-required-packages)
- [Install CKAN into a Python virtual environment](#2-install-ckan-into-a-python-virtual-environment)
- [Setup a PostgreSQL database](#3-setup-a-postgresql-database)
- [Install and configure Solr](#4-install-and-configure-solr)
- [Create a CKAN config file](#5-create-a-ckan-config-file)
- [Link to who.ini](#6-link-to-whoini)
- [Create database tables](#7-create-database-tables)
- [Set up the DataStore](#8-set-up-the-datastore)
- [You're done!](#9-youre-done)

<br> 



<!-- ------------------------------------------ -->
## Requirements for CKAN
[Installing CKAN from source](http://docs.ckan.org/en/2.9/maintaining/installing/install-from-source.html)

For Python 3 installations, the minimum Python version required is 3.6
- Ubuntu 20.04 includes Python 3.8 as part of its distribution (most tested version in ZLE)
- Ubuntu 18.04 includes Python 3.6 as part of its distribution
- Ubuntu 16.04 includes Python 3.5 as part of its distribution

<br>



<!-- ------------------------------------------ -->
## 1. Install the required packages
If you’re using a Debian-based operating system (such as Ubuntu) install the required packages with this command:
```
sudo apt-get install python3-dev postgresql libpq-dev python3-pip python3-venv git-core solr-jetty openjdk-8-jdk redis-server
```

If you’re not using a Debian-based operating system, find the best way to install the following packages on your operating system (see our [How to Install CKAN](https://github.com/ckan/ckan/wiki/How-to-Install-CKAN) wiki page for help):

| Package       | Description                                                                           |
| ---           | ---                                                                                   |
| Python        | The Python programming language, v3.6 or newer (or v2.7)                              |
| PostgresSQL   | The PostgreSQL database system, v9.5 or newer                                         |
| libpq         | The C programmer’s interface to PostgreSQL                                            |
| pip           | A tool for installing and managing Python packages                                    |
| python3-venv  | The Python3 virtual environment builder (or for Python 2 use ‘virtualenv’ instead)    |
| Git           | A distributed version control system                                                  |
| Apache Solr   | A search platform                                                                     |
| Jetty         | An HTTP server (used for Solr)                                                        |
| OpenJDK JDK   | The Java Development Kit (used by Jetty)                                              |
| Redis         | An in-memory data structure store                                                     |
<br>



<!-- ------------------------------------------ -->
## 2. Install CKAN into a Python virtual environment
> **Tipp:**
If you’re installing CKAN for development and want it to be installed in your home directory, you can symlink the directories used in this documentation to your home directory. This way, you can copy-paste the example commands from this documentation without having to modify them, and still have CKAN installed in your home directory:
>

```
mkdir -p ~/ckan/lib
sudo ln -s ~/ckan/lib /usr/lib/ckan
mkdir -p ~/ckan/etc
sudo ln -s ~/ckan/etc /etc/ckan
```
<br>


### a. Create a Python virtual environment (virtualenv) to install CKAN into, and activate it
```
sudo mkdir -p /usr/lib/ckan/default
sudo chown `whoami` /usr/lib/ckan/default
python3 -m venv /usr/lib/ckan/default
. /usr/lib/ckan/default/bin/activate
```

> The final command above activates your virtualenv. The virtualenv has to remain active for the rest of the installation and deployment process, or commands will fail. You can tell when the virtualenv is active because its name appears in front of your shell prompt, something like this:
```
(default) $ _
```
For example, if you logout and login again, or if you close your terminal window and open it again, your virtualenv will no longer be activated. You can always reactivate the virtualenv with this command:
```
. /usr/lib/ckan/default/bin/activate
```
<br>


### b. Install the recommended `setuptools` version and up-to-date pip
```
pip install setuptools==44.1.0
pip install --upgrade pip
```
<br>


### c. Install the CKAN source code into your virtualenv
To install the current stable release of CKAN (CKAN 2.9.8), run:
```
pip install -e 'git+https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git@ckan-2.9.8#egg=ckan[requirements]'
```
<br>

> The installation via GitLab and GitHub fails at the moment. One workaround could be using `pip install wheel` and trying the installation with GitLab again.


### d. Deactivate and reactivate your virtualenv, to make sure you’re using the virtualenv’s copies of commands like `ckan` rather than any system-wide installed copies
```
deactivate
. /usr/lib/ckan/default/bin/activate
```
<br>



<!-- ------------------------------------------ -->
## 3. Setup a PostgreSQL database
Check that PostgreSQL was installed correctly by listing the existing databases:
```
sudo -u postgres psql -l
```

Check that the encoding of databases is `UTF8`, if not you might find issues later on with internationalisation. Since changing the encoding of PostgreSQL may mean deleting existing databases, it is suggested that this is fixed before continuing with the CKAN install.

Next you’ll need to create a database user if one doesn’t already exist. Create a new PostgreSQL user called ckan_default, and enter a password for the user when prompted. You’ll need this password later:
```
sudo -u postgres createuser -S -D -R -P ckan_default
```

Create a new PostgreSQL database, called ckan_default, owned by the database user you just created:
```
sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
```
<br>



<!-- ------------------------------------------ -->
## 4. Install and configure Solr
Install Solr, running this command in a terminal:
```
sudo apt install -y solr-tomcat
```
<br>


### a. Change the default port Tomcat runs on (8080) to the one expected by CKAN.
To do so change the following line in the `/etc/tomcat9/server.xml` file:
-> `sudo nano /etc/tomcat9/server.xml`

```
From:
  <Connector port="8080" protocol="HTTP/1.1"

To:
  <Connector port="8983" protocol="HTTP/1.1"
```
<br>


### b. Replace the default `schema.xml` file with a symlink to the CKAN schema file included in the sources.
```
sudo mv /etc/solr/conf/schema.xml /etc/solr/conf/schema.xml.bak
sudo ln -s /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
```
If this causes an error in the next step, copy the file by
```
sudo cp /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
```
<br>


### c. Now restart Solr (use `tomcat8` on older Ubuntu versions):
```
sudo service tomcat9 restart
```
Check that Solr is running by opening http://localhost:8983/solr/

> localhost probably needs to be changed into an ip adress

<br> 



<!-- ------------------------------------------ -->
## 5. Create a CKAN config file
Create a directory to contain the site’s config files:
```
sudo mkdir -p /etc/ckan/default
sudo chown -R `whoami` /etc/ckan/
```

Create the CKAN config file:
```
ckan generate config /etc/ckan/default/ckan.ini
```
<br>


In the `ckan.ini` following options need to be changed:
&rarr; `sudo nano /etc/ckan/default/ckan.ini` 

<br>

### **ckan.devserver.host**
what does it do? unknown &rarr; Todo: find out \
But it hast to be changed from localhost to the using ip (&rarr; Todo: what does it mean for the offis vm?)
```
ckan.devserver.host = ip address
```
<br>



### **sqlalchemy.url**
This should refer to the database we created in [3. Setup a PostgreSQL database](#3-setup-a-postgresql-database) database above:
```
sqlalchemy.url = postgresql://ckan_default:pass@localhost/ckan_default
```
Replace `pass` with the password that you created in 3. Setup a PostgreSQL database above.

<br>


### **site_id**
Each CKAN site should have a unique `site_id`, for example:
```
ckan.site_id = default
```
<br>


### **site_url**
Provide the site’s URL (used when putting links to the site into the FileStore, notification emails etc). For example:
```
ckan.site_url = http://localhost:5000
alternate: ckan.site_url = http://demo.ckan.org
```
`demo.ckan.org` is only a placeholder

<br>


### **sorl_url**
Set the solr url with the new Port:
```
solr_url=http://127.0.0.1:8983/solr
```
<br>



<!-- ------------------------------------------ -->
## 6. Link to `who.ini`
`who.ini` (the Repoze.who configuration file) needs to be accessible in the same directory as your CKAN config file, so create a symlink to it:
```
ln -s /usr/lib/ckan/default/src/ckan/who.ini /etc/ckan/default/who.ini
```
<br>



<!-- ------------------------------------------ -->
## 7. Create database tables
Now that you have a configuration file that has the correct settings for your database, you can create the database tables:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini db init
```
You should see `Initialising DB: SUCCESS`

>  **Tipp:** If the command prompts for a password it is likely you haven’t set up the `sqlalchemy.url` option in your CKAN configuration file properly. See [5. Create a CKAN config file](#5-create-a-ckan-config-file).

<br>



<!-- ------------------------------------------ -->
## 8. Set up the DataStore
> ToDo

<br>



<!-- ------------------------------------------ -->
## 9. You’re done!
You can now run CKAN from the command-line. This is a simple and lightweight way to serve CKAN that is useful for development and testing:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini run
```
Open [http://localhost:5000/](http://localhost:5000/) in a web browser, and you should see the CKAN front page.

Now that you’ve installed CKAN, you should:
- Run CKAN’s tests to make sure that everything’s working, see [Testing CKAN](http://docs.ckan.org/en/2.9/contributing/test.html).
- If you want to use your CKAN site as a production site, not just for testing or development purposes, then deploy CKAN using a production web server such as uWSGI or Nginx. See [Deploying a source install](http://docs.ckan.org/en/2.9/maintaining/installing/deployment.html).
- Begin using and customizing your site, see [Getting started](http://docs.ckan.org/en/2.9/maintaining/getting-started.html).