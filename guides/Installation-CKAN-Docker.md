# Installation of CKAN with Docker Compose
<!-- ------------------------------------------ -->

This chapter is a tutorial on how to install the latest CKAN (master or any stable version) with Docker Compose. The scenario shown here is one of many possible scenarios and environments in which CKAN can be used with Docker.

This chapter aims to provide a simple, yet fully customizable deployment - easier to configure than a source install, more customizable than a package install.

The discussed setup can be useful as a development / staging environment; additional care has to be taken to use this setup in production.

> Some design decisions are opinionated (see notes), which does not mean that the alternatives are any worse. Some decisions may or may not be suitable for production scenarios, e.g. the use of CKAN master. Notably, this tutorial does not use Docker Swarm; additional steps may need to be taken to adapt the setup to use Docker Swarm.

<br>


### Table of contents
- [Enviroment](#enviroment----set-up-docker)
  - [Set up Docker repository](#set-up-docker-repository)
  - [Install Docker Engine](#install-docker-engine)
  - [Docker Engine post-installation steps](#docker-engine-post-installation-steps)
  - [Install the Compose plugin](#install-the-compose-plugin)
  - [Clone ckan-docker GitHub](#ckan-docker-github)
- [Build Docker images](#build-docker-images)
- [Changing the base image](#changing-the-base-image)
- [Common Error](#common-error)
- [Future ToDo's](#future-todos)

<br> 



<!-- ------------------------------------------ -->
## Enviroment -> Set up Docker
This tutorial was tested on Ubuntu 20.04 LTS. The hosts can be local environments or cloud VMs. It is assumed that the user has direct access (via terminal / ssh) to the systems and root permissions.



<!-- ------------------------------------------ -->
### Set up Docker repository
Before you install Docker Engine for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

<br>


#### a. Update the `apt` package index and install packages to allow apt to use a repository over HTTPS:
```
sudo apt-get update
```
```
sudo  apt-get install \
      ca-certificates \
      curl \
      gnupg \
      lsb-release
```
<br> 


#### b. Add Docker’s official GPG key:
```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
<br> 


#### c. Use the following command to set up the repository:
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
<br> 



<!-- ------------------------------------------ -->
### Install Docker Engine

#### a. Update the `apt` package index:
```
sudo apt-get update
```
<br> 


#### b. Install Docker Engine, containerd, and Docker Compose.
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
<br> 


#### c. Verify that the Docker Engine installation is successful by running the hello-world image:
```
sudo docker run hello-world
```
<br>



<!-- ------------------------------------------ -->
### Docker Engine post-installation steps

#### a. Create the `docker` group.
```
sudo groupadd docker
```
<br> 


#### b. Add your user to the `docker` group.
```
sudo usermod -aG docker $USER
```
<br> 


#### c. Log out and log back in so that your group membership is re-evaluated.
> If you’re running Linux in a virtual machine, it may be necessary to restart the virtual machine for changes to take effect.

```
newgrp docker
```
<br> 


#### d. Verify that you can run `docker` commands without `sudo`.
```
docker run hello-world
```

The output should be following:
```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
<br> 



<!-- ------------------------------------------ -->
### Install the Compose plugin
> From the end of June 2023 Compose V1 won’t be supported anymore and will be removed from all Docker Desktop versions.

Make sure you switch to [Compose V2](https://docs.docker.com/compose/compose-file/) with the `docker compose` CLI plugin or by activating the Use **Docker Compose V2** setting in Docker Desktop. For more information, see the [Evolution of Compose](https://docs.docker.com/compose/compose-v2/)

<br> 


#### a. Update the package index, and install the latest version of Docker Compose:
```
sudo apt-get update
sudo apt-get install docker-compose-plugin
```
<br> 


#### b. Verify that Docker Compose is installed correctly by checking the version.
```
docker compose version
```
Output should be `Docker Compose version vN.N.N` where `vN.N.N` should be the latest version.

<br>



<!-- ------------------------------------------ -->
### Clone ckan-docker GitHub
For the installation of CKAN we need to clone the repository of ckan-docker into a directory of our choice:
```
cd /path
git clone https://github.com/ckan/ckan-docker.git
```

Rename the directory to ckan (optional):
```
mv ckan-docker/ ckan/
```
<br>



<!-- ------------------------------------------ -->
## Build Docker images
In this step we will build the Docker images and create Docker data volumes with user-defined, sensitive settings (e.g. database passwords).

<br>

#### a. Sensitive settings and environment variables
Copy `.env.example` to `.env` inside your `ckan-docker` folder and follow instructions within to set passwords and other sensitive or user-defined variables.
```
mv .env.example .env
```
with `sudo nano .env` is the file openable

Following changes should be done:
- `NGINX_SSLPORT_HOST` to 443
- `CKAN_VERSION` to the version needed (current version on VM2: ckan-2.10.0)
- `CKAN_SITE_URL` to the needed address
- more ToDo's

<br> 


### b. Build the images
Inside the CKAN directory:
```
docker compose build
```

After building the docker it is possible to start it:
```
docker compose up
```

You also can start it in the detached mode:
```
docker compose up -d
```
<br>


## Changing the base image
The base image used in the CKAN Dockerfile and Dockerfile.dev can be changed so a different DockerHub image is used eg: ckan/ckan-base:2.9.9 could be used instead of ckan/ckan-base:2.10.
<br>

## Common Error
Error:
```
Error response from daemon: driver failed programming external connectivity on endpoint PROCESS (CONTAINER-ID): Error starting userland proxy: listen tcp4 0.0.0.0:PORT: bind: address already in use
```
Solution: 
```
sudo fuser -k PORT/tcp
```


## Future ToDo's
- .env changes
- intern development and production images
- extensions
- patches?