# Fast commands for CKAN
<!-- ------------------------------------------ -->
This document describes important fast commands for the usage of CKAN

### Table of contents
- [How to restart CKAN](#how-to-restart-ckan)
- [How to stop the CKAN Terminal](#how-to-stop-the-ckan-terminal)
- [remove and add who.ini](#remove-and-add-whoini)
- [Kill Ports in Ubuntu](#kill-ports-in-ubuntu)
- [Source install troubleshooting](#source-install-troubleshooting)
    - [Solr setup troubleshooting](#solr-setup-troubleshooting)
    - [AttributeError: ‘module’ object has no attribute ‘css/main.debug.css’](#attributeerror-module-object-has-no-attribute-cssmaindebugcss¶)
    - [Using the debug mode](#using-the-debug-mode)

<br> 



<!-- ------------------------------------------ -->
## How to restart CKAN
change the path:
```
. /usr/lib/ckan/default/bin/activate

```

Start ckan:
```
cd /usr/lib/ckan/default/src/ckan
ckan -c /etc/ckan/default/ckan.ini run
```

> CKAN should start after.

<br>



<!-- ------------------------------------------ -->
## How to stop the CKAN Terminal
CKAN should be stoppable with `Ctrl + C`

<br> 



<!-- ------------------------------------------ -->
## Important Config Files
For CKAN in general:
```
sudo nano /etc/ckan/default/ckan.ini
```

For deployment:
```
sudo nano /etc/nginx/sites-available/ckan
```

<br> 



<!-- ------------------------------------------ -->
## remove and add who.ini
```
rm /etc/ckan/default/who.ini
ln -s /usr/lib/ckan/default/src/ckan/who.ini /etc/ckan/default/who.ini
```
<br>



<!-- ------------------------------------------ -->
## remove an non-empty folder (for new installations or debugging)
```
cd TO/PATH/WHERE/FOLDER/IS/INSIDE
rm -r FOLDER-NAME
```
<br>



<!-- ------------------------------------------ -->
## Kill Ports in Ubuntu
If you see the Error `OSError: [Errno 98] Address already in use` you need to kill the port for CKAN being able to start again
```
sudo kill -9 $(sudo lsof -t -i:5000)
```
<br>



<!-- ------------------------------------------ -->
## Source install troubleshooting
> Todo

<br>


<!-- ------------------------------------------ -->
### Solr setup troubleshooting
> Todo

<br>


<!-- ------------------------------------------ -->
### AttributeError: ‘module’ object has no attribute ‘css/main.debug.css’¶
> Todo

<br>


<!-- ------------------------------------------ -->
### Using the debug mode
CKAN has a build-in debug mode. It can be actvated in the `ckan.ini` file (`sudo nano /etc/ckan/default/ckan.ini`)
```
debug = true
```

after activating the debug mode you might see a `ImportError: No module named ‘flask_debugtoolbar’` Error in the terminal. Simply install the development requirements:
```
pip install -r /usr/lib/ckan/default/src/ckan/dev-requirements.txt
```
<br>