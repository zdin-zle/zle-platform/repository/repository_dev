# How to update CKAN
<!-- ------------------------------------------ -->
This document describes the different types of CKAN release, and explains how to upgrade a site to a newer version of CKAN.

### Table of contents
- [CKAN release](#ckan-releases)
- [Update CKAN (Repository)](#update-ckan--repository-)
- [Update CKAN (Source)](#update-ckan--source-)
  - [Backup your database](#backup-your-database)
  - [Upgrade CKAN](#upgrade-ckan)

<br> 


<!-- ------------------------------------------ -->
## CKAN releases
CKAN follows a predictable release cycle so that users can depend on stable releases of CKAN, and can plan their upgrades to new releases.
Each release has a version number of the form `M.m` (eg. 2.1) or `M.m.p` (eg. 1.8.2), where `M` is the **major version**, `m` is the **minor version** and `p` is the **patch version** number. There are three types of release:

<br> 


#### **Major Releases**
Major releases, such as CKAN 1.0 and CKAN 2.0, increment the major version number. These releases contain major changes in the CKAN code base, with significant refactorings and breaking changes, for instance in the API or the templates. These releases are very infrequent.

<br>


#### **Minor Releases**
Minor releases, such as CKAN 1.8 and CKAN 2.1, increment the minor version number. These releases are not as disruptive as major releases, but backwards-incompatible changes may be introduced in minor releases. The Changelog will document any breaking changes. We aim to release a minor version of CKAN roughly every three months.

<br>


#### **Patch Releases**
Patch releases, such as CKAN 1.8.1 or CKAN 2.0.1, increment the patch version number. These releases do not break backwards-compatibility, they include only bug fixes and security fixes, ensured to be non-breaking. Patch releases do not contain:
- Database schema changes or migrations
- Function interface changes
- Plugin interface changes
- New dependencies (unless absolutely necessary)
- Big refactorings or new features in critical parts of the code



<!-- ------------------------------------------ -->
## Update CKAN (Repository)
First you need to check if the CKAN GitHub is linked as a upstream with `git remote -v`. It should show following output:
```
origin          https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git (fetch)
origin          https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git (push)
upstream        https://github.com/ckan/ckan.git (fetch)
upstream        https://github.com/ckan/ckan.git (push)

```

If not, add CKAN as a upstream repository:
```
git remote add upstream https://github.com/ckan/ckan.git
```

Next step, fetch the changes of CKAN and create a local branch which has the same name as the tag version of ckan. In this step it is important that the names are the same.
```
git fetch upstream
git checkout -b ckan-VERSION ckan-VERSION
```

In the last step, the new branch needs to be pushed to the repository.
```
git branch --set-upstream-to=origin/ckan-VERSION ckan-VERSION
git push origin
```
if `git push origin` gives an error try to delete the tag with `git tag -d ckan-VERSION` and try again

<br>



<!-- ------------------------------------------ -->
## Update CKAN (Source)
This document describes the different types of CKAN release, and explains how to upgrade a site to a newer version of CKAN.

<br>


### Backup your database
> TODO

<br>


### Upgrade CKAN
This section will walk you through the steps to upgrade your CKAN site to a newer version of CKAN.

<br>


#### **Step 1**
Check the Changelog for changes regarding the required 3rd-party packages and their minimum versions (e.g. web, database and search servers) and update their installations if necessary.

<br>


#### **Step 2**
Activate your virtualenv and switch to the ckan source directory, e.g.:
```
. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
```
<br>


#### **Step 3**
Checkout the new CKAN version from git, for example:
```
git fetch
git checkout ckan-VERSION
```
If a normal checkout is not possible do a force checkout
```
git checkout -f ckan-VERSION
```

If you have any CKAN extensions installed from source, you may need to checkout newer versions of the extensions at this point as well. Refer to the documentation for each extension.

<br>


#### **Step 4**
Update CKAN’s dependencies:
```
pip install --upgrade -r requirements.txt
```
<br>


#### **Step 5**
Register any new or updated plugins:
```
python setup.py develop
```
<br>


#### **Step 6**
If there have been changes in the Solr schema (check the Changelog to find out) you need to restart tomcat for the changes to take effect:
```
sudo service tomcat9 restart
```
<br>


#### **Step 7**
If there have been changes in the database schema (check the Changelog to find out) you need to upgrade your database schema.

<br>


#### **Step 8**
If new configuration options have been introduced (check the Changelog to find out) then check whether you need to change them from their default values. See Configuration Options for details.

<br>


#### **Step 9**
Rebuild your search index by running the `ckan search-index rebuild` command:
```
ckan -c /etc/ckan/default/ckan.ini search-index rebuild -r
```
<br>


#### **Step 10**
Finally, restart your web server!
```
ckan -c /etc/ckan/default/ckan.ini run
```

> You’re done!
