# Installation of CKAN 2.10.0 and greater (Source)

> **WARNING:** <ins>No working</ins> installation guide yet. Local installation failed with a <ins>permission error</ins>: \
> `PermissionError: [Errno 13] Permission denied: '/var/lib/ckan'` \
> `ckan` Folder is not existing in `/var/lib/`

### Table of contents
- [Requirements](#requirements-for-ckan)
- [Installation of Docker (for Solr and Redis)](#installation-of-docker--for-solr-and-redis-)
- [Install the required packages](#1-install-the-required-packages)
- [Install CKAN into a Python virtual environment](#2-install-ckan-into-a-python-virtual-environment)

<br> 



<!-- ------------------------------------------ -->
## Requirements for CKAN
[Installing CKAN from source](https://docs.ckan.org/en/2.10/maintaining/installing/install-from-source.html)

For Python 3 installations, the minimum Python version required is 3.8
- Ubuntu 22.04 includes Python 3.10 as part of its distribution
- Ubuntu 20.04 includes Python 3.8 as part of its distribution (VM1 and VM2 are both Ubuntu 20.04)

<br>
 


<!-- ------------------------------------------ -->
## Installation of Docker (for Solr and Redis)
For later installation Docker is a better option to install Solr and Redis. A complete instruction how to install Docker is given in the [Installation of CKAN with Docker Compose](../guides/Installation-CKAN-Docker.md) guide.

<br>



<!-- ------------------------------------------ -->
## 1. Install the required packages
If you’re using a Debian-based operating system (such as Ubuntu) install the required packages with this command:
```
sudo apt-get install python3-dev libpq-dev python3-pip python3-venv git-core redis-server
```

If you’re not using a Debian-based operating system, find the best way to install the following packages on your operating system (see our [How to Install CKAN](https://github.com/ckan/ckan/wiki/How-to-Install-CKAN) wiki page for help):

| Package       | Description                                                                                                                  |
| ---           |------------------------------------------------------------------------------------------------------------------------------|
| Python        | [The Python programming language, v3.7 or newer](https://www.python.org/getit/)                                              |
| PostgresSQL   | [The PostgreSQL database system, v10 or newer](https://www.postgresql.org/docs/10/libpq.html)                                |
| libpq         | [The C programmer’s interface to PostgreSQL](https://www.postgresql.org/docs/8.1/libpq.html)                                 |
| pip           | [A tool for installing and managing Python packages](https://pip.pypa.io/en/stable/)                                         |
| python3-venv  | [The Python3 virtual environment builder (or for Python 2 use ‘virtualenv’ instead)](https://virtualenv.pypa.io/en/latest/)  |
| Git           | [A distributed version control system](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)                        |
| Apache Solr   | [A search platform](https://solr.apache.org/)                                                                                |
| Jetty         | [An HTTP server](https://www.eclipse.org/jetty/) (used for Solr)                                                             |
| OpenJDK JDK   | [The Java Development Kit](https://openjdk.org/install/) (used by Jetty)                                                     |
| Redis         | [An in-memory data structure store](https://redis.io/)                                                                       |
<br>



<!-- ------------------------------------------ -->
## 2. Install CKAN into a Python virtual environment
> **Tipp:**
If you’re installing CKAN for development and want it to be installed in your home directory, you can symlink the directories used in this documentation to your home directory. This way, you can copy-paste the example commands from this documentation without having to modify them, and still have CKAN installed in your home directory:
>

```
mkdir -p ~/ckan/lib
sudo ln -s ~/ckan/lib /usr/lib/ckan
mkdir -p ~/ckan/etc
sudo ln -s ~/ckan/etc /etc/ckan
```
<br>


### a. Create a Python virtual environment (virtualenv) to install CKAN into, and activate it
```
sudo mkdir -p /usr/lib/ckan/default
sudo chown `whoami` /usr/lib/ckan/default
python3 -m venv /usr/lib/ckan/default
. /usr/lib/ckan/default/bin/activate
```

> The final command above activates your virtualenv. The virtualenv has to remain active for the rest of the installation and deployment process, or commands will fail. You can tell when the virtualenv is active because its name appears in front of your shell prompt, something like this:
```
(default) $ _
```
For example, if you logout and login again, or if you close your terminal window and open it again, your virtualenv will no longer be activated. You can always reactivate the virtualenv with this command:
```
. /usr/lib/ckan/default/bin/activate
```
<br>


### b. Install the recommended `setuptools` version and up-to-date pip
```
pip install --upgrade pip
```
<br>


### c. Install the CKAN source code into your virtualenv
To install the current stable release of CKAN (CKAN 2.9.7), run:
```
pip install -e 'git+https://gitlab.com/zdin-zle/zle-platform/repository/repository_dev.git@ckan-2.10.0#egg=ckan[requirements]'
```
<br>


### d. Deactivate and reactivate your virtualenv, to make sure you’re using the virtualenv’s copies of commands like `ckan` rather than any system-wide installed copies
```
deactivate
. /usr/lib/ckan/default/bin/activate
```
<br>



<!-- ------------------------------------------ -->
## 3. Setup a PostgreSQL database
Check that PostgreSQL was installed correctly by listing the existing databases:
```
sudo -u postgres psql -l
```

Check that the encoding of databases is `UTF8`, if not you might find issues later on with internationalisation. Since changing the encoding of PostgreSQL may mean deleting existing databases, it is suggested that this is fixed before continuing with the CKAN install.

Next you’ll need to create a database user if one doesn’t already exist. Create a new PostgreSQL user called ckan_default, and enter a password for the user when prompted. You’ll need this password later:
```
sudo -u postgres createuser -S -D -R -P ckan_default
```

Create a new PostgreSQL database, called ckan_default, owned by the database user you just created:
```
sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
```
<br>



<!-- ------------------------------------------ -->
### 4. Setup Solr